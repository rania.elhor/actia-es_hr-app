package com.RHActia.actia_app.Services;

import com.RHActia.actia_app.model.Employee;
import com.RHActia.actia_app.services.EmployeeService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class EmployeeControllerSeleniumTest {
    private WebDriver driver;
    private EmployeeService employeeService;

    @Before
    public void setUp() {
        // Set up WebDriver
        System.setProperty("webdriver.chrome.driver", "C:\\webdrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); // Implicit wait
        employeeService = new EmployeeService(); // Initialize your service
    }

    @After
    public void tearDown() {
        // Quit WebDriver
        driver.quit();
    }

    @Test
    public void testAddEmployee() throws IOException {
        // Test adding an employee
        Employee employeeToAdd = new Employee(); // Create an employee object with required fields
        employeeToAdd.setFirstname("John");
        employeeToAdd.setLastname("Doe");
        employeeToAdd.setEmail("john.doe@example.com");
        // Set other required fields as necessary

        // Call the addEmployee method of your service
        Employee addedEmployee = employeeService.addEmployee(employeeToAdd);

        // Assert that the employee was added successfully
        // You can check if the added employee matches the employee you created and added
        assertEquals(employeeToAdd, addedEmployee);
    }

    @Test
    public void testGetAllEmployees() {
        // Test getting all employees
        // Call the getAllEmployees method of your service
        List<Employee> allEmployees = employeeService.getAllEmployees();

        // Assert that the list of employees is not null and contains at least one employee
        assertNotNull(allEmployees);
        assertFalse(allEmployees.isEmpty());
    }

    @Test
    public void testDeleteEmployee() {
        // Test deleting an employee
        int employeeIdToDelete = 1; // Specify the ID of the employee to delete

        // Call the deleteEmployeeByID method of your service
        boolean isDeleted = employeeService.deleteEmployeeByID(employeeIdToDelete);

        // Assert that the employee was deleted successfully
        assertTrue(isDeleted);
    }
}
